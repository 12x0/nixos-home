{ pkgs, lib, config, ...}: {
  nixpkgs.config.allowUnfree = true;

  imports = [ ./software/vim.nix ./software/git.nix ];

  home.username = "0x21";

  home.sessionVariables = rec {
    EDITOR = "nvim";
    VISUAL = EDITOR;
  };

  home.packages = let
    unstable = import <nixos-unstable> { config.allowUnfree = true; };
    php = (pkgs.php81.withExtensions ({ enabled, all }: enabled ++ [ all.xdebug all.imagick ]));
  in with pkgs; [ 
    # unstable packages
    unstable.xplr

    # charm stuff
    glow
    unstable.vhs
    
    libnotify

    # browsers
    firefox-devedition-bin
    librewolf
    ungoogled-chromium
    tor

    # tools
    xclip 
    brightnessctl
    playerctl
    rofi
    imagemagick
    yubioath-desktop
    udiskie

    gnumake
    go-task # gnumake alternative

    # shit software I need to use for now
    spotify
    zoom-us 
    discord 
    tdesktop

    go_1_18
    gcc

    dogdns # dig alternative
    dua # du alternative
    bat # cat
    rm-improved # rip, aliased as rm
    ripgrep # rg

    (nerdfonts.override { fonts = [ "FiraCode" ]; })
    neofetch 
    docker 

    viu # image viewer for the terminal
    feh # image viewer, not for the terminal 

    # webdev
    php
    php.packages.composer
    nodejs-18_x 
    yarn
    python38

    # writing
    onlyoffice-bin
  ];

  fonts.fontconfig.enable = lib.mkForce true;

  programs.ssh = import ./software/ssh.nix;

  programs.exa.enable = true;

  programs.zsh = import ./software/zsh/zsh.nix;

  programs.atuin = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      style = "compact";
    };
  };

  programs.starship = {
    enable = true;
    enableZshIntegration = true;
  };

  home.file.".config/starship.toml".source = ./software/starship/starship.toml;
 home.file.".npmrc".source = ./software/npm/npmrc;
  home.file.".config/i3/config".source = ./software/i3/i3-config;
  home.file.".config/i3/polybar.sh".source = ./software/polybar/polybar.sh;
  home.file.".config/xplr/init.lua".source = ./software/xplr/xplr.lua;
  home.file.".config/rofi/config.rasi".source = ./software/rofi/config.rasi;
  home.file.".background-image".source = ./wallpapers/background-image.png;

  programs.gpg.enable = true;

  programs.gh.enable = true;

  programs.zoxide = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.fzf = {
    enable = true;
    enableZshIntegration = true;
  };

  programs.tmux = {
    enable = true;
    clock24 = true;
  };

  services.gpg-agent = {
    enable = true;
    enableZshIntegration = true;
    enableSshSupport = true;
  };

  services.picom.enable = true;

  services.playerctld.enable = true;

  services.polybar = rec {
    enable = true;
    package = pkgs.polybar.override {
      i3GapsSupport = true;
    };
    script = "";
    config = import ./software/polybar/polybar.nix;
  };

  services.dunst.enable = true;

  programs.command-not-found.enable = true;

  programs.alacritty = import ./software/alacritty.nix;

  services.udiskie.enable = true;

  services.flameshot = {
    enable = true;

    settings = {
      # if you need to change that, run flameshot config, and grab th econfig at ~/.config/flameshot/flameshot.ini
      General.buttons = ''@Variant(\0\0\0\x7f\0\0\0\vQList<int>\0\0\0\0\v\0\0\0\x2\0\0\0\x3\0\0\0\b\0\0\0\t\0\0\0\x10\0\0\0\n\0\0\0\v\0\0\0\r\0\0\0\x17\0\0\0\xe\0\0\0\f)'';
      General.savePath = "/home/0x21/Screenshots";
      General.showDesktopNotification = false;
      General.uiColor = "#ff9c00";
    };
  };
}

