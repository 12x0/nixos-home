# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix <home-manager/nixos>];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.cleanTmpDir = true;

  networking.hostName = "0x21-sys"; # Define your hostname.

  virtualisation.docker.enable = true;

  networking.networkmanager.enable = true;

  time.timeZone = "Europe/Paris";

  i18n.defaultLocale = "en_US.utf8";
  security.pam.services.gdm.enableGnomeKeyring = true;
  services.xserver = {
    enable = true;
    layout = "fr";
    displayManager = {
      defaultSession = "none+i3";

      lightdm.greeters.mini = {
        enable = true;
        user = "0x21";
        extraConfig = ''
          [greeter]
          show-password-label = false
          password-alignment = center
          show-image-on-all-monitors = true
          [greeter-theme]
          background-image = "/etc/lightdm/background-image.png"
          border-width = 0px
          password-border-width = 0px
          layout-space = 0
        '';
      };
    };

    libinput = {
      touchpad.naturalScrolling  = false;
      enable = true;
    };

    windowManager.i3 = {
      package = pkgs.i3-gaps;
      enable = true;
    };
    
    excludePackages = [pkgs.xterm];
  };

  console.keyMap = "fr";

  services.printing.enable = true;

  sound.enable = true;
  hardware.pulseaudio.enable = false;
  hardware.bluetooth.enable = true;
  security.rtkit.enable = true;
  security.pam.yubico = {
    enable = true;
    debug = true;
    mode = "challenge-response";
  };

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  services.blueman.enable = true;

  users.users."0x21" = {
    description = "0x24D560";
    createHome = true;
    home = "/home/0x21";
    isNormalUser = true;
    extraGroups = [ "wheel" "video" "docker" ];
    shell = pkgs.zsh;
  };

  environment.etc = {
    "lightdm/background-image.png" = {
      source = ./wallpapers/login-background-image.png;
      user = "lightdm";
      mode = "0777";
    };
  };
  environment.shellInit = ''
    export GPG_TTY="$(tty)"
    gpg-connect-agent /bye
    export SSH_AUTH_SOCK="/run/user/$UID/gnupg/S.gpg-agent.ssh"
  '';

  home-manager.users."0x21" = import ./home.nix; 

  services.pcscd.enable = true;

  services.tlp = {
    enable = true;
    settings = {
      TLP_ENABLE = true;
    };
  };

  services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="${pkgs.coreutils}/bin/chgrp video /sys/class/backlight/intel_backlight/brightness"
   ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="intel_backlight", RUN+="${pkgs.coreutils}/bin/chmod g+w /sys/class/backlight/%k/brightness"
  '';
  services.udev.packages = [ pkgs.yubikey-personalization ];

  system.stateVersion = "22.05";

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };
}
