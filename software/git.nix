{ config, pkgs, ...}:

{
  programs.git = {
    enable = true;
    attributes = [ "* text=auto" ];
    delta.enable = true;
    ignores = [ 
      ".idea"
      ".vscode"
      "__pycache__"
      "venv"
    ];
    lfs.enable = true;

    userEmail = "git@0x21.be";
    userName = "0x21";

    extraConfig = {
      init.defaultBranch = "main";
      core = { whitespace = "trailing-space,space-before-tab"; };
      color = { ui = "auto"; };
      merge = { ff = "only"; };
      rebase = { autoSquash = "true"; };
      github = { user = "0x21"; };
    };

    includes = [
      { 
        condition = "gitdir:~/Code/Old/";
        contents.user = {
          email = "github@felixdorn.fr";
          name = "Félix Dorn";
        };
        contents.commit = { gpgSign = false; };
      }
    ];
  };
}
