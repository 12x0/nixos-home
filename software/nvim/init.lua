vim.opt.relativenumber = true

vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smartindent = true

-- Search is casing-insentive
vim.opt.ic = true

-- No line-wrapping
vim.opt.wrap = true

vim.cmd("setlocal spell spelllang=en_us")
-- Scheme
-- vim.cmd("colorscheme onedark")
