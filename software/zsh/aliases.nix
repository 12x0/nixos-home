{
  ssh = "TERM=xterm-256color ssh";
  amend = "git commit --amend";
  ls = "exa --group-directories-first";
  sl = "ls";
  l = "ls -l";
  la = "ls -la";
  e = "exit";
  rm = "rip";
  cat = "bat -p";
  dua = "dua i $(pwd)/*";
  cd = "z";
  dig = "dog";
  copy = "xclip -sel clip";
  paste = "xclip -o";
  o = "phpstorm ."; # old habits

  gs = "gss";

  a = "php artisan";
  m = "php artisan migrate";
  mf = "php artisan migrate:fresh";
  mfs = "php artisan migrate:fresh --seed";

  es = "cd /etc/nixos; sudo -E vim /etc/nixos";
  s = "sudo nixos-rebuild switch";

  E = "echo 'É' | copy";
}
