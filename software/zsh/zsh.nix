{
  enable = true;
  enableCompletion = true;
  enableAutosuggestions = true;
  enableSyntaxHighlighting = true;

  dotDir = ".config/zsh";

  shellAliases = import ./aliases.nix;
  
  sessionVariables = {
    MANPAGER = "sh -c 'col -bx | bat -l man -p'";
  };

  zplug = {
    zplugHome = /home/0x21/.config/zplug;
    enable = true;
    plugins = [
      { name = "djui/alias-tips"; }
      { name = "chisui/zsh-nix-shell"; }
      { name = "marlonrichert/zsh-autocomplete"; }
    ];
  };

  oh-my-zsh = {
    enable = true;

    plugins = [ "extract" "sudo" "git" ];
  };

  initExtra = ''
    unalias _
  '';

  history = rec {
    size = 1000000;
    save = size;
  };
}
