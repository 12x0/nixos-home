{
  enable = true;
  matchBlocks = {
    "plane" = {
      hostname = "37.187.105.125";
    };

    "main" = {
      hostname = "54.38.192.161";
    };

    "fpam" = {
      hostname = "ssh.cluster021.hosting.ovh.net";
      user = "fairepluuf";
    };

    "tino" = {
      hostname = "54.36.101.180";
      user = "debian";
    };
  };
}
