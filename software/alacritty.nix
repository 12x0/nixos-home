{
  enable = true;
  settings = {
    window = {
      padding = { y = 10; x = 10; };

      opacity = 0.8;
    };

    font = rec {
      size = 6;
      normal.family = "FiraCode Nerd Font";
      bold.family = normal.family;
      italic.family = normal.family;
    };

    colors = {
      primary = {
        background = "#000000";
        foreground = "#abb2bf";
      };

      normal = {
        black = "#5c6370";
        red = "#e06c75";
        green = "#98c379";
        yellow = "#e5c07b";
        blue = "#61afef";
        magenta = "#c678dd";
        cyan = "#56b6c2";
        white = "#abb2bf";
      };

      bright = {
        black = "#5c6370";
        red = "#e06c75";
        green = "#98c379";
        yellow = "#e5c07b";
        blue = "#61afef";
        magenta = "#c678dd";
        cyan = "#56b6c2";
        white = "#ffffff";
      };
    };
  };
}
