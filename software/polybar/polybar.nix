let 
  black = "#af000000"; 
  white = "#ffffff";
in {
  "bar/top" = {
    width = "98.8%";
    height = "24";

    modules-left = "isep cpu isep memory isep sep i3";
    modules-center = "prev sep current-song sep next";
    modules-right = "wired-network sep wireless-network sep backlight sep alsa sep battery sep date sep lock sep reboot sep power";
    background = "#00000000";
    fixed-center = true;

    bottom = true;

    offset-y = 10;
    offset-x = 12;

    override-redirect = true;
    wm-restack = "i3";

    font-0 = "FiraCode Nerd Font:size=10;2";
  };

  "module/isep" = {
    type = "custom/text";
    content-background = black;
    content = " ";
  };

  "module/i3" = {
    type = "internal/i3";
    format = "<label-state> <label-mode>";
    index-sort = true;
    wrapping-scroll = false;

    label-focused = "%index%";
    label-focused-background = black;
    label-focused-padding = 1;

    label-unfocused = "%index%";
    label-unfocused-foreground = black; 
    label-unfocused-padding = 1;
  };

  "module/memory" = {
    type = "internal/memory";
    format-background = black;
    format = "MEM <label>";
  };

  "module/cpu" = {
    type = "internal/cpu";
    format-background = black;
    format = "CPU <label>";
  };

  "module/backlight" = {
    type = "internal/backlight";

    card = "intel_backlight";

    enable-scroll = true;

    format = "<ramp> <label>";

    format-background = black;
    format-foreground = "#ffffff";
    format-padding = 1;

    ramp-0 = "";
    ramp-1 = "";
    ramp-2 = "";
    ramp-3 = "";
  };

  "module/alsa" = {
    type = "internal/alsa";

    format-volume = "<ramp-volume> <label-volume>";
    format-volume-background = black;
    format-volume-foreground = "#ffffff";
    format-volume-padding = 1;

    ramp-volume-0 = "奄";
    ramp-volume-1 = "奔";
    ramp-volume-2 = "墳";

    label-muted = "婢 muted";
    label-muted-foreground = "#ffffff";
    label-muted-background = black;
    label-muted-padding = 1;
  };

  "module/sep" = {
    type = "custom/text";
    content = " ";
  };

  "module/date" = {
    type = "internal/date";
    poll-interval = 1;

    format-background = black;
    format-foreground = "#ffffff";
    format-padding = 1;
    date-alt = "%d-%m-%Y";
    time = "%H:%M";
    time-alt = " %H:%M:%S";

    label = "%date%%time%";
  };

  "module/battery" = {
    type = "internal/battery";
    battery = "BAT0";
    adapter = "ADP1";
    full-at = 98;
    interval = 1;

    format-charging = " <label-charging>";
    format-charging-background = black;
    format-charging-foreground = white;
    format-charging-padding = 1;

    format-discharging = " <label-discharging>";
    format-discharging-background = black;
    format-discharging-foreground = white;
    format-discharging-padding = 1;

    format-full-background = black;
    format-full-foreground = white;
    format-full-padding = 1;
  };

  "module/prev" = {
    type = "custom/script";
    exec = ''echo  "玲"'';
    format = "<label>";
    click-left = "playerctl previous";
    format-background = black;
    format-foreground = white;
    format-padding = 1;
  };

  "module/next" = {
    type = "custom/script";
    exec = ''echo "怜"'';
    format = "<label>";
    click-left = "playerctl next";
    format-background = black;
    format-foreground = white;
    format-padding = 1;
  };

  "module/current-song" = {
    type = "custom/script";
    tail = true;
    interval = 1;
    format = "<label>";
    exec = ''playerctl metadata --format "{{ title }}" | cut -c 1-20'';  
    format-background = black;
    format-foreground = white;
    format-padding = 1;
    click-left = "playerctl play-pause";
  };

  "module/power" = {
    type = "custom/text";
    content = "襤";
    content-background = black;
    content-foreground = white;
    content-padding = 1;
    click-left = ''shutdown -P 0'';
  };

  "module/lock" = {
    type = "custom/text";
    content = "";
    content-background = black;
    content-foreground = white;
    content-padding = 1;
    click-left = ''dm-tool lock'';
  };

  "module/reboot"=  {
    type = "custom/text";

    content = "ﰇ";

    content-padding = 1;
    content-background = black;
    content-foreground = white;


    click-left = ''reboot'';
  };

  "module/wired-network" = {
    type = "internal/network";

    interface-type = "wired";

    label-connected = "%downspeed:5%";
    label-disconnected = "no ethernet";
    
    format-connected = "<label-connected>";
    format-disconnected = "<label-disconnected>";
    format-packetloss = "ﯲ <label-connected>";

    format-connected-background = black; 
    format-disconnected-background = black; 
    format-packetloss-background = black;

    format-connected-padding = 1;
    format-disconnected-padding = 1;
    format-packetloss-padding = 1;

  };

  "module/wireless-network" = {
    type = "internal/network";

    interface-type = "wireless";

    label-connected = "%downspeed:5%";
    label-disconnected = "no wifi";

    format-connected = "<label-connected>";
    format-disconnected = "<label-disconnected>";
    format-packetloss = "ﯲ <label-connected>";

    format-connected-background = black; 
    format-disconnected-background = black; 
    format-packetloss-background = black;

    format-connected-padding = 1;
    format-disconnected-padding = 1;
    format-packetloss-padding = 1;
  };

  settings =  {
    screenchange-reload = true;
  };
}
